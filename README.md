# RESTng Web Service Template

## Installation

```bash
composer create-project --repository=https://satis.itapps.miamioh.edu/miamioh miamioh/restng-template <project-name>
```

## Configuration

### composer.json

 1. `name` must be global unique in the [satis](https://satis.itapps.miamioh.edu/miamioh) and [packagist](https://packagist.org/)
 2. namespace in the `autoload` and `autoload-dev` section must be global unique in the [satis](https://satis.itapps.miamioh.edu/miamioh) and [packagist](https://packagist.org/)

### resource.php

Example:
```php
return [
    'resources' => [
        'courseSection' => [
            MiamiOH\CourseSectionWebService\Resources\CourseSectionResourceProvider::class,
            MiamiOH\CourseSectionWebService\Resources\ParticipantResourceProvider::class
        ]
    ]
];
```
## Add New Service

 1. create a new resource provider in the `src/Resources` directory (see [ExampleResourceProvider.php](./examples/ExampleResourceProvider.php)).
 2. register resource provider in the `resource.php`.
 3. create a new service in the `src/Services` directory (see [Example1Service.php](./examples/Example1Service.php)).
 
## Test

### Unit Test

 1. unit tests should put in the `tests/Unit` directory (see [ExampleTest](./examples/ExampleTest.php)).
 2. run it by `./vendor/bin/phpunit` if phpunit is not installed globally
 3. code coverage can be found in the `tests/coverage`

### Feature Test

...

## Deployment

 1. add project to [satis](https://satis.itapps.miamioh.edu/miamioh) through puppet
 2. add project to [restng-addons-admin](https://git.itapps.miamioh.edu/solution-delivery-dev/restng-addons-admin)
 3. build and deploy by [Jenkin](https://ci.itapps.miamioh.edu/jenkins/job/SolutionDelivery/job/Development/job/RESTng-Addons-Admin/)