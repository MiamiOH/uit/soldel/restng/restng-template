<?php

namespace MiamiOH\RestngTemplate\Resources;

use MiamiOH\RESTng\App;

class ExampleResourceProvider extends \MiamiOH\RESTng\Util\ResourceProvider
{
    /**
     * @var string
     */
    private $serviceName = 'Example.Example1';
    /**
     * @var string
     */
    private $tag = "example";
    /**
     * @var string
     */
    private $resourceRoot = "example.v1.example1";
    /**
     * @var string
     */
    private $patternRoot = "/example/v1/example1";
    /**
     * @var string
     */
    private $classPath = 'MiamiOH\RestngTemplate\Services\Example1Service';

    public function registerDefinitions(): void
    {
        $this->addTag(array(
            'name' => $this->tag,
            'description' => 'example tag'
        ));

        $this->addDefinition([
            'name' => $this->resourceRoot . '.Object',
            'type' => 'object',
            'properties' => [
                'sampleAttribute' => [
                    'type' => 'string',
                    'description' => 'this is a sample attribute'
                ]
            ]
        ]);

        $this->addDefinition(array(
            'name' => $this->resourceRoot . '.Object.Collection',
            'type' => 'array',
            'items' => array(
                '$ref' => '#/definitions/' . $this->resourceRoot . '.Object',
            )
        ));
    }

    public function registerServices(): void
    {

        $this->addService(array(
            'name' => $this->serviceName,
            'class' => $this->classPath,
            'description' => 'example service',
            'set' => array(
                'databaseFactory' => array(
                    'type' => 'service',
                    'name' => 'APIDatabaseFactory'
                )
            ),
        ));


    }

    public function registerResources(): void
    {

        $this->addResource(array(
                'action' => 'read',
                'name' => $this->resourceRoot . '.get',
                'description' => 'example resource',
                'summary' => 'example resource',
                'pattern' => $this->patternRoot,
                'service' => $this->serviceName,
                'method' => 'get',
                'tags' => [$this->tag],
                'returnType' => 'model',
                'responses' => array(
                    App::API_OK => array(
                        'description' => 'Collection of classrooms',
                        'returns' => array(
                            'type' => 'model',
                            '$ref' => '#/definitions/' . $this->resourceRoot . '.Object.Collection',
                        )
                    )
                )
            )
        );
    }

    public function registerOrmConnections(): void
    {

    }
}
