<?php

namespace MiamiOH\LocationWebService\Services;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Service;

class BuildingService extends Service
{
    /**
     * @var DBH
     */
    private $dbh;
    /**
     * @var string
     */
    private $datasource_name = 'MUWS_GEN_PROD';

    public function setDatabaseFactory(DatabaseFactory $databaseFactory)
    {
        $this->dbh = $databaseFactory->getHandle($this->datasource_name);
    }

    public function get()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $results = $this->dbh->queryall_array('select * from stvterm');
        $response->setPayload($results);

        return $response;
    }
}
