<?php

namespace MiamiOH\RestngTemplate\Tests\Unit;

use MiamiOH\RESTng\Connector\DatabaseFactory;
use MiamiOH\RESTng\Legacy\DB\DBH;
use MiamiOH\RESTng\Service;
use MiamiOH\RESTng\Util\Request;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\RESTng\Util\User;
use PHPUnit\Framework\MockObject\MockObject;

class ExampleTest extends \MiamiOH\RESTng\Testing\TestCase
{
    /**
     * @var MockObject
     */
    private $request;
    /**
     * @var Service
     *
     * This SHOULD change to your service
     */
    private $service;
    /**
     * @var MockObject
     */
    private $user;
    /**
     * @var MockObject
     */
    private $dbh;

    protected function setUp()
    {
        parent::setUp();

        // mock request
        $this->request = $this->createMock(Request::class);

        // mock api user
        $this->user = $this->createMock(User::class);

        // mock database handler
        $this->dbh = $this->createMock(DBH::class);

        // create service
        $this->service = new Service(); // It should be replaced by your service

        $this->service->setRequest($this->request);
        $this->service->setApiUser($this->user);

        // if your service uses database
        $databseFactory = $this->createMock(DatabaseFactory::class);
        $databseFactory->method('getHandle')->willReturn($this->dbh);

        // method name 'setDatabaseFactory' is based on how you specify
        // injectable service in the resource provider
        $this->service->setDatabaseFactory($databseFactory);
    }

    public function sampleTest()
    {
        $dataFromDatabase = [
            [
                'column1' => 'value1',
                'column2' => 'value2'
            ]
        ];

        $yourExpectedData = [
            // something else
        ];

        $this->dbh->method('queryall_array')->willReturn($dataFromDatabase);

        /**
         * @var Response $response
         * */
        $response = $this->service->yourMethod();

        $this->assertEquals($yourExpectedData, $response->getPayload());
    }
}